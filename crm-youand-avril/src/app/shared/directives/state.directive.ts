import { Directive, HostBinding, Input, OnChanges } from '@angular/core';
import { StateClient } from '../enums/state-client.enum';
import { StateOrder } from '../enums/state-order.enum';

@Directive({
  selector: '[appState]'
})
export class StateDirective implements OnChanges {
@Input() appState: StateOrder | StateClient;
@HostBinding('class') nomClass: string; // Donne accès au td et modifi la class/id/name etc ...

  constructor() { }

  ngOnChanges(): void {
    this.nomClass = this.formatClass(this.appState);
  }

  private formatClass(state: StateOrder | StateClient): string {
    return `state-${state.normalize('NFD').replace(/[\u0300-\u036f\s]/g, '').toLowerCase()}`;
  }

}
