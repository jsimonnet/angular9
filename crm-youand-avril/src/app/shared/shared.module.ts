import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TemplatesModule } from '../templates/templates.module';
import { BtnComponent } from './components/btn/btn.component';
import { TableDarkComponent } from './components/table-dark/table-dark.component';
import { TableLightComponent } from './components/table-light/table-light.component';
import { StateDirective } from './directives/state.directive';
import { TotalPipe } from './pipe/total.pipe';
import { TotalttcPipe } from './pipe/totalttc.pipe';
import { NavInlineComponent } from './components/nav-inline/nav-inline.component';



@NgModule({
  declarations: [TotalPipe, TotalttcPipe, StateDirective, TableLightComponent, TableDarkComponent, BtnComponent, NavInlineComponent],
  imports: [
    CommonModule,
    TemplatesModule,
    RouterModule
  ],
  exports: [TotalPipe, TotalttcPipe, StateDirective, TableLightComponent, TableDarkComponent, TemplatesModule, BtnComponent, NavInlineComponent]
})
export class SharedModule { }
