import { Component, OnInit, Input } from '@angular/core';
import { LinksItems } from '../../interfaces/links-items';

@Component({
  selector: 'app-nav-inline',
  templateUrl: './nav-inline.component.html',
  styleUrls: ['./nav-inline.component.scss']
})
export class NavInlineComponent implements OnInit {
@Input() itemsLinks: LinksItems[];
  constructor() { }

  ngOnInit(): void {
  }

}
