import { Pipe, PipeTransform } from '@angular/core';
import { Client } from '../models/client';

@Pipe({
  name: 'totalttc'
})
export class TotalttcPipe implements PipeTransform {

  transform(value: number, ...args: any[]): number {
    // console.log(value);
    // console.log(args);
    if (value) {
      return value * (1 + 20/100);
    }
    return null;
  }

}
