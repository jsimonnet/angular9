import { StateClient } from '../enums/state-client.enum';
import { ClientI } from '../interfaces/client-i';

export class Client implements ClientI {
  state = StateClient.ACTIVE;
  id: number;
  name: string;
  ca: number;
  constructor(obj?: Partial<Client>) { // l'objet doit répondre partielement à Order
    if (obj) {
      Object.assign(this, obj)
    }
  }
}
