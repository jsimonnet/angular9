export interface LinksItems {
  route: string;
  label: string;
}
