import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Client } from 'src/app/shared/models/client';
import { ClientsService } from '../../services/clients.service';
import { Order } from 'src/app/shared/models/order';

@Component({
  selector: 'app-page-add-client',
  templateUrl: './page-add-client.component.html',
  styleUrls: ['./page-add-client.component.scss']
})
export class PageAddClientComponent implements OnInit {
  public title: String;
  public subtitle: String;
  public initItem = new Client();

  constructor (
    private cs: ClientsService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.title = 'Client';
    this.subtitle = 'Add a Client';
  }

  public onSubmited(item: Client) {
    this.cs.add(item).subscribe((res) => {
      // traite res si errors ou pas
      // console.log(res);
      // ex de rediraction classique
      // this.router.navigate(['orders']);
      // ex de redirection relative par rapport à la route sur laquelle on est
      this.router.navigate(['../'], { relativeTo: this.route });
    });
  }

}
