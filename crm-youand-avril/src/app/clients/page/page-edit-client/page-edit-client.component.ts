import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Client } from 'src/app/shared/models/client';
import { ClientsService } from '../../services/clients.service';

@Component({
  selector: 'app-page-edit-client',
  templateUrl: './page-edit-client.component.html',
  styleUrls: ['./page-edit-client.component.scss']
})

export class PageEditClientComponent implements OnInit {
  public title: String;
  public subtitle: String;
  public selectedItem: Client;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private cs : ClientsService
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.cs.getItemById(params.get('id')).subscribe((rep) => {
        console.log(rep);
        this.selectedItem = rep;
      })
    }),

    this.route.data.subscribe((datas) => {
      this.title = datas.title;
      this.subtitle = datas.subtitle;
    })
  }

  public onSubmited(item: Client) {
    this.cs.update(item).subscribe((res) => {
      console.log(res);
      this.router.navigate(['clients']);
    });
  }

}
