import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { StateClient } from 'src/app/shared/enums/state-client.enum';
import { LinksItems } from 'src/app/shared/interfaces/links-items';
import { Client } from 'src/app/shared/models/client';
import { ClientsService } from '../../services/clients.service';

@Component({
  selector: 'app-page-list-clients',
  templateUrl: './page-list-clients.component.html',
  styleUrls: ['./page-list-clients.component.scss']
})

export class PageListClientsComponent implements OnInit {
  public headers: String[];
  public title: String;
  public subtitle: String;
  public collection$: Subject<Client[]> = new Subject();
  public states = Object.values(StateClient); //transforme un objet en tableau
  public open = false;
  public selectedItem: Client;
  public verticalNavLink: LinksItems[];

  constructor(
    private cs: ClientsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.verticalNavLink = [
      {
        route: 'name',
        label: 'Name'
      },
      {
        route: 'ca',
        label: 'Total sales'
      },
    ];
    this.title = 'Clients';
    this.subtitle = 'All clients';
    this.majCollection();
    this.headers = [
      'Action',
      'state',
      'name',
      'caHt',
      'caTtc',
    ];
  }

  public changeState(item: Client, event) {
    this.cs.changeState(item, event.target.value).subscribe((res) => {
      item.state = res.state;
      // maj data formulaire (itemSelected) (v2)
      this.selectedItem = res;
    })
  }

  //Open form and give item to the form
  public edit(item: Client) {
    this.router.navigate(['clients', 'edit', item.id]);
  }

  //V2
  public editForm(item: Client) {
    this.open = true;
    this.selectedItem = item;
  }

  //close form and use orderService to upadte item collection
  public onSubmited(item: Client) {
    console.log(item);
    this.cs.update(item).subscribe((res) => {
      console.log(res);
      this.open = false;
      this.majCollection();
    });
  }

  public delete(item: Client) {
    this.cs.delete(item.id).subscribe((res) => {
      // console.log(res);
      // maj collection$ coté front à partir d'un appel http
      this.majCollection();
    })
  }

  public majCollection() {
    this.cs.collection.subscribe((col) => {
      this.collection$.next(col);
    })
  }

  public getItem(item: Client) {
    this.cs.client$.next(item);
  }
}
