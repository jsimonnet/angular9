import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageListClientsComponent } from './page/page-list-clients/page-list-clients.component';
import { PageAddClientComponent } from './page/page-add-client/page-add-client.component';
import { PageEditClientComponent } from './page/page-edit-client/page-edit-client.component';
import { CaClientComponent } from './components/ca-client/ca-client.component';
import { NameClientComponent } from './components/name-client/name-client.component';

const routes: Routes = [
  {
    path: '',
    component: PageListClientsComponent,
    children: [
      { path: '', redirectTo: 'name', pathMatch: 'full' },
      { path: 'name', component: NameClientComponent },
      { path: 'ca', component: CaClientComponent },

    ],
  },
  { path: 'add', component: PageAddClientComponent },
  { path: 'edit/:id',
    component: PageEditClientComponent,
    data: { title: 'Client', subtitle: 'Edit an client' }
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ClientsRoutingModule { }
