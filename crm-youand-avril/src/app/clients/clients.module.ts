import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { ClientsRoutingModule } from './clients-routing.module';
import { FormClientsComponent } from './components/form-clients/form-clients.component';
import { PageAddClientComponent } from './page/page-add-client/page-add-client.component';
import { PageListClientsComponent } from './page/page-list-clients/page-list-clients.component';
import { IconsModule } from '../icons/icons.module';
import { PageEditClientComponent } from './page/page-edit-client/page-edit-client.component';
import { NameClientComponent } from './components/name-client/name-client.component';
import { CaClientComponent } from './components/ca-client/ca-client.component';



@NgModule({
  declarations: [PageListClientsComponent, PageAddClientComponent, FormClientsComponent, PageEditClientComponent, NameClientComponent, CaClientComponent],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    IconsModule
  ]
})
export class ClientsModule { }
