import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { StateClient } from 'src/app/shared/enums/state-client.enum';
import { Client } from 'src/app/shared/models/client';

@Component({
  selector: 'app-form-clients',
  templateUrl: './form-clients.component.html',
  styleUrls: ['./form-clients.component.scss']
})
export class FormClientsComponent implements OnChanges {

  @Input() item : Client;
  @Output() submited: EventEmitter<Client> = new EventEmitter();
  public states = Object.values(StateClient);
  public form: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnChanges(): void {
    state : [];
    id: [];
    name: [];
    ca: [];
    this.form = this.fb.group({
      state: [this.item.state],
      name: [this.item.name],
      ca: [this.item.ca],
      id: [this.item.id],
    });
  }

  public onSubmit() {
    this.submited.emit(this.form.value);
  }
}
