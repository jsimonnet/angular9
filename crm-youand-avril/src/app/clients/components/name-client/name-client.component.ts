import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Client } from 'src/app/shared/models/client';
import { ClientsService } from '../../services/clients.service';

@Component({
  selector: 'app-name-client',
  templateUrl: './name-client.component.html',
  styleUrls: ['./name-client.component.scss']
})
export class NameClientComponent implements OnInit {
  public client$: BehaviorSubject<Client>;
  constructor(private cs: ClientsService) { }

  ngOnInit(): void {
    this.client$ = this.cs.client$;
  }

}
