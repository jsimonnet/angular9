import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaClientComponent } from './ca-client.component';

describe('CaClientComponent', () => {
  let component: CaClientComponent;
  let fixture: ComponentFixture<CaClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
