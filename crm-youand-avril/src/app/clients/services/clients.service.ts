import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { StateClient } from 'src/app/shared/enums/state-client.enum';
import { Client } from 'src/app/shared/models/client';
import { environment } from 'src/environments/environment';
import { Order } from 'src/app/shared/models/order';

@Injectable({
  providedIn: 'root'
})

export class ClientsService {
  private pCollection :Observable<Client[]>;
  private urlApi = environment.urlApi; // Récupère l'url de la bdd (src/environment/environment.ts)
  public client$: BehaviorSubject<Client> = new BehaviorSubject(null);
  private lastIdItemDeleted: number;
  constructor(private http: HttpClient) { // Inject le service httpClient qui permet de communiquer avec la bdd
    this.collection = this.http.get<Client[]>(`${this.urlApi}clients`).pipe (
      map((tab) => {
        if (this.client$.value === null) {
           this.client$.next(tab[0]);
        } else if (this.lastIdItemDeleted === this.client$.value.id) {
          this.client$.next(null);
        }

        return tab.map((obj) => {
          return new Client(obj);
        });
      })
    );
  }

  get collection(): Observable<Client[]> {
    return this.pCollection;

  }
  set collection(col: Observable<Client[]>) {
    this.pCollection = col;

  }

  public changeState(item: Client, state: StateClient): Observable<Client> {
    const obj = new Client({...item});
    obj.state = state;
    return this.update(obj);
  }

  public update(item: Client): Observable<Client> {
    return this.http.patch<Client>(`${this.urlApi}clients/${item.id}`, item) // Récupère par son ID pour modifier l'item selectionné

  }

  public add(item: Client): Observable<Client> {
    return this.http.post<Client>(`${this.urlApi}clients`, item);
  }

  public getItemById(id: string): Observable<Client> {
    return this.http.get<Client>(`${this.urlApi}clients/${id}`);
  }

  public delete(id: number): Observable<Client> {
    this.lastIdItemDeleted= id;
    return this.http.delete<Client>(`${this.urlApi}clients/${id}`);
  }
}
