import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ui', // creation du selecteur qui sera appelé dans la vue (app.component.html)
  templateUrl: './ui.component.html', // récupération du html de la vue spécifique au component
  styleUrls: ['./ui.component.scss'], // récupération du style de la vue spécifique au component
})
export class UiComponent implements OnInit {
  public open = true;
  constructor() {}

  ngOnInit(): void {}

  public toggle() {
    this.open = !this.open;
  }
}
