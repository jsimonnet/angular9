import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from 'src/app/shared/models/order';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { StateOrder } from 'src/app/shared/enums/state-order.enum';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  private pCollection :Observable<Order[]>;
  private urlApi = environment.urlApi;

  constructor(private http: HttpClient) {
    // this.collection = this.http.get<Order[]>(this.urlApi +'orders'); AVANT ES6
    this.collection = this.http.get<Order[]>(`${this.urlApi}orders`).pipe( //DEPUIS ES6
      map((tab) => {
        return tab.map((obj)=>{
          return new Order(obj);
        });
      })
    );
  }

  get collection(): Observable<Order[]> {
    return this.pCollection;
  }
  set collection(col: Observable<Order[]>) {
    this.pCollection = col;
  }

  // change state of item
  public changeState(item: Order, state: StateOrder): Observable<Order> { // Recupère le state de base et récupère le state modifier
    // console.log(item);
    const obj = new Order({...item});
    obj.state = state;
    // console.log(item);
    return this.update(obj);

  }


  // update item in collection
  public update(item: Order): Observable<Order> {
   return this.http.patch<Order>(`${this.urlApi}orders/${item.id}`, item) // Récupère par son ID pour modifier l'item selectionné

  }

  // add item in collection
  public add(item: Order): Observable<Order> {
   return this.http.post<Order>(`${this.urlApi}orders`, item) // Récupère par son ID pour modifier l'item selectionné
  }

  public getItemById(id: string): Observable<Order> {
    return this.http.get<Order>(`${this.urlApi}orders/${id}`)
  }

  public delete(id: number): Observable<Order> {
    return this.http.delete<Order>(`${this.urlApi}orders/${id}`)
  }

}
