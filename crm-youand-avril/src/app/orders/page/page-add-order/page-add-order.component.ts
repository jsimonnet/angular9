import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Order } from 'src/app/shared/models/order';
import { OrdersService } from '../../services/orders.service';

@Component({
  selector: 'app-page-add-order',
  templateUrl: './page-add-order.component.html',
  styleUrls: ['./page-add-order.component.scss']
})
export class PageAddOrderComponent implements OnInit {
  public title: String;
  public subtitle: String;
  public initItem = new Order();

  constructor (
    private os :OrdersService,
    private router: Router,
    private route : ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe((datas) => {
      this.title = datas.title;
      this.subtitle = datas.subtitle;
    })

  }

  public onSubmited(item: Order) {
    this.os.add(item).subscribe((res) => {
      // traite res si errors ou pas
      // console.log(res);
      // ex de rediraction classique
      // this.router.navigate(['orders']);
      // ex de redirection relative par rapport à la route sur laquelle on est
      this.router.navigate(['../'], {relativeTo: this.route});
    });
  }
}
