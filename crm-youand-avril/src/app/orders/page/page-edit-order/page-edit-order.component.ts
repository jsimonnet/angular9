import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Order } from 'src/app/shared/models/order';
import { OrdersService } from '../../services/orders.service';


@Component({
  selector: 'app-page-edit-order',
  templateUrl: './page-edit-order.component.html',
  styleUrls: ['./page-edit-order.component.scss']
})
export class PageEditOrderComponent implements OnInit {
  public title: String;
  public subtitle: String;
  public selectedItem: Order;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private os: OrdersService

  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.os.getItemById(params.get('id')).subscribe((rep) => {
        console.log(rep);
        this.selectedItem = rep;
      })
    }),

    this.route.data.subscribe((datas) => {
      this.title = datas.title;
      this.subtitle = datas.subtitle;
    })
  }

  public onSubmited(item: Order) {
    this.os.update(item).subscribe((res) => {
      console.log(res);
      this.router.navigate(['orders']);
    });
  }

}
