import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { StateOrder } from 'src/app/shared/enums/state-order.enum';
import { Order } from 'src/app/shared/models/order';
import { OrdersService } from '../../services/orders.service';

@Component({
  selector: 'app-page-list-orders',
  templateUrl: './page-list-orders.component.html',
  styleUrls: ['./page-list-orders.component.scss']
})
export class PageListOrdersComponent implements OnInit {
  public headers: String[];
  public title: String;
  public subtitle: String;
  public collection$: Subject<Order[]> = new Subject();
  public states = Object.values(StateOrder);
  public open = false;
  public selectedItem: Order;

  constructor(
    private os: OrdersService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.title = 'Orders';
    this.subtitle = 'All orders';
    // this.sub = this.os.collection.subscribe((datas) => {
    //    console.log(datas);
    //   this.collection = datas;
    // });
    this.majCollection();
    this.headers = [
      'Action',
      'State',
      'Type',
      'Client',
      'Nbjours',
      'TjmHt',
      'TotalHt',
      'TotalTtc'
    ];
  }

  public changeState(item: Order, event) {
    // console.log(event.target.value);

    this.os.changeState(item, event.target.value).subscribe((res) => {
      // res étant la réponse de l'api (flux de data)
      // console.log(res);
      item.state = res.state;
      // maj data formulaire (itemSelected) (v2)
      this.selectedItem = res;
    });

  }

  public addForm() {
    this.open = true;
    this.selectedItem = new Order();
  }

  public edit(item: Order) {
    this.router.navigate(['orders', 'edit', item.id]);
  }

  public editForm(item: Order) {
    this.open = true; // au click ouvre modal form
    this.selectedItem = item;
  }

  public onSubmited(item: Order) {
    console.log(item);
    if(item.id) {
      this.os.update(item).subscribe((res) => {
        this.open = false;
        this.majCollection();
      });
    } else {
      this.os.add(item).subscribe((res) => {
        this.open = false;
        this.majCollection();
      });
    }
  }

  public delete(item: Order) {
    this.os.delete(item.id).subscribe((res) => {
      // console.log(res);
      // maj collection$ coté front à partir d'un appel http
      this.majCollection();
    })
  }
  public majCollection() {
    this.os.collection.subscribe((col) => {
      this.collection$.next(col);
    })
  }

}
