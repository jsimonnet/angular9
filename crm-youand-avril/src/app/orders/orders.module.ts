import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { FormOrderComponent } from './components/form-order/form-order.component';
import { OrdersRoutingModule } from './orders-routing.module';
import { PageAddOrderComponent } from './page/page-add-order/page-add-order.component';
import { PageListOrdersComponent } from './page/page-list-orders/page-list-orders.component';
import { IconsModule } from '../icons/icons.module';
import { PageEditOrderComponent } from './page/page-edit-order/page-edit-order.component';



@NgModule({
  declarations: [PageListOrdersComponent, PageAddOrderComponent, FormOrderComponent, PageEditOrderComponent],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    IconsModule
  ]
})
export class OrdersModule { }
