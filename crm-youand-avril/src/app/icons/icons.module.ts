import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NavComponent } from './components/nav/nav.component';
import { CloseComponent } from './components/close/close.component';
import { DeleteComponent } from './components/delete/delete.component';
import { EditComponent } from './components/edit/edit.component';



@NgModule({
  declarations: [NavComponent, CloseComponent, DeleteComponent, EditComponent],
  exports: [NavComponent, CloseComponent, DeleteComponent, EditComponent], // rend ce component public dans les components déclaré dans l'export
  imports: [CommonModule, FontAwesomeModule],
})
export class IconsModule {}
